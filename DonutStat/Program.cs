﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Telegram.Bots;
using Telegram.Bots.Extensions.Polling;
using Quartz;
using Telegram.Bots.Extensions.Polling.Configs;
using Telegram.Bots.Types;
using YooMoney;
using Telegram.Bots.Requests;
using YooMoney.Model;
using Z.Dapper.Plus;
using DonutStat.Dal;
using DonutStat.Bll;
using DonutStat.Model;

namespace DonutStat
{
    class Program
    {
        private static async Task Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
            await Task.CompletedTask;
        }

        /// <summary>
        /// Host builder method
        /// </summary>
        /// <param name="args">launch arguments</param>
        /// <returns>returs host builder</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json")
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json");
                    config.AddEnvironmentVariables("SIM_");

                    if (args != null)
                    {
                        config.AddCommandLine(args);
                    }
                })
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                })
                .ConfigureServices((hostContext, sc) =>
                {
                    var config = (IConfigurationRoot)hostContext.Configuration;
                    var quartzOptions = config.GetSection(nameof(Model.QuartzOptions)).Get<Model.QuartzOptions>();
                    sc.AddQuartz(q =>
                    {
                        q.SchedulerId = "Yoo_Scheduler";
                        q.UseMicrosoftDependencyInjectionJobFactory();
                        q.ScheduleJob<YooStateJob>(trigger => trigger
                            .StartNow()
                            .WithSimpleSchedule(x => x.WithIntervalInSeconds(quartzOptions.IntervalInSeconds).RepeatForever()));
                        q.AddJob<YooStateJob>(j => j.StoreDurably());
                    });

                    sc.AddQuartzHostedService(options => { options.WaitForJobsToComplete = true; });
                    sc.AddHostedService<YooHostService>();

                    sc.AddTransient<IBankClient, YooClient>();
                    var yooKey = config.GetSection("YooKey").Get<string>();
                    sc.AddHttpClient<IBankClient, YooClient>(client =>
                    {
                        client.BaseAddress = new Uri("https://yoomoney.ru/api/");
                        client.Timeout = new TimeSpan(0, 0, 30);
                        client.DefaultRequestHeaders.Add("Accept", "application/json");
                        client.DefaultRequestHeaders.Add("Authorization", $"Bearer {yooKey}");
                    });


                    var tBotSection = config.GetSection(nameof(TbotOptions));
                    var tBot = tBotSection.Get<TbotOptions>();
                    sc.Configure<TbotOptions>(tBotSection);
                    IServiceProvider provider = sc.AddPolling<TelegramHandler>()
                        .AddBotClient(tBot.Key)
                        .Services
                        .BuildServiceProvider();

                    IBotClient bot = provider.GetRequiredService<IBotClient>();
                    bot.HandleAsync
                    (
                        new SetMyCommands(new List<BotCommand>()
                        {
                            new BotCommand() {Command = "/balance", Description = "Остаток на счёте"},
                            new BotCommand() {Command = "/rating", Description = "Рейтинг донатеров"}
                        })
                    );

                    var directory = AppDomain.CurrentDomain.BaseDirectory;
                    var fileName = $"Data Source={directory}/donutStat.sqlite";

                    sc.AddTransient<IYooDbService, YooDbService>();
                    sc.AddTransient<IYooDbProvider, SqliteYooDbProvider>(_ => new SqliteYooDbProvider(fileName));
                });
    }
}