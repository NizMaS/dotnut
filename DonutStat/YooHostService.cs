using Microsoft.Extensions.Hosting;
using Quartz;

namespace DonutStat
{
    public class YooHostService: BackgroundService
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private IScheduler _scheduler;

        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="schedulerFactory">Фабрика задача Quartz</param>
        public YooHostService(ISchedulerFactory schedulerFactory)
        {
            _schedulerFactory = schedulerFactory ?? throw new ArgumentNullException(nameof(schedulerFactory));
        }

        /// <inheritdoc />
        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            if (_scheduler != null)
            {
                await _scheduler.Shutdown(cancellationToken);
            }

            await base.StopAsync(cancellationToken);
        }

        /// <inheritdoc />
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();
            _scheduler = await _schedulerFactory.GetScheduler(stoppingToken);

            await _scheduler.Start(stoppingToken);

            await Task.CompletedTask;
        }
    }
}