using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Quartz;
using YooMoney;
using YooMoney.Model;
using DonutStat.Bll;
using System.Text;
using Telegram.Bots.Requests;
using Telegram.Bots;
using DonutStat.Model;
using Microsoft.Extensions.Options;

namespace DonutStat
{
    [DisallowConcurrentExecution]
    public class YooStateJob : IJob
    {
        private readonly IYooDbService _service;
        private readonly IBotClient _bot;
        private readonly TbotOptions _options;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="client">Экземпляр клиента сервиса юмани</param>
        public YooStateJob(IYooDbService service, IBotClient bot, IOptions<TbotOptions> botOps)
        {
            _service = service ?? throw new ArgumentNullException(nameof(service));
            _bot = bot ?? throw new ArgumentNullException(nameof(bot));
            _options = botOps?.Value ?? throw new ArgumentNullException(nameof(botOps));
        }

        /// <summary>
        /// Метод, вызываемый по графику
        /// </summary>
        /// <param name="context">Контекст шедуллера</param>
        public async Task Execute(IJobExecutionContext context)
        {
            var opsStr = await _service.RefreshOperationsAsync();
            foreach (var opStr in opsStr)
            {
                await _bot.HandleAsync(new SendText(_options.ChatId, opStr));
            }            
        }
    }
}