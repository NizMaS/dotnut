﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IYooDbProvider.cs" company="TETA">
// Copyright (c) TETA. Ufa, 2020.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// <summary>
//  Интерфейс провайдера доступа к локальной бд
// </summary>

using System.Collections.Generic;
using YooMoney.Model;

namespace DonutStat.Dal
{
    /// <summary>
    /// Интерфейс провайдера доступа к локальной бд
    /// </summary>
    public interface IYooDbProvider
    {
        /// <summary>
        /// Добавить новую запись в бд
        /// </summary>
        /// <param name="msg">Сообщение из wits0</param>
        Task<List<Operation>> AddOperationsAsync(IList<Operation> operations);

        /// <summary>
        /// Получить все не отправленные сообщения
        /// </summary>
        /// <returns>Список сообщений</returns>
        Task<IList<Operation>> Get();

        Task<string> GetRatingAsync();
    }
}