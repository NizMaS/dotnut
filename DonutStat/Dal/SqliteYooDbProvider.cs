﻿using System.Text;
// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqliteYooDbProvider.cs" company="TETA">
// Copyright (c) TETA. Ufa, 2020.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// <summary>
//  Провайдер доступа к локальной бд
// </summary>

using System;
using System.Collections.Generic;
using Dapper;
using Microsoft.Data.Sqlite;
using YooMoney.Model;

namespace DonutStat.Dal
{
    /// <summary>
    /// Провайдер доступа к локальной бд
    /// </summary>
    public class SqliteYooDbProvider : IYooDbProvider
    {
        private const string InsertQuery = $@"INSERT INTO operations (groupId, operationId, title, amount, direction, datetime, status, type, amountCurrency, isSbpOperation, spendingCategory)
                                                VALUES (@{nameof(Operation.GroupId)},
                                                        @{nameof(Operation.OperationId)},
                                                        @{nameof(Operation.Title)},
                                                        @{nameof(Operation.Amount)},
                                                        @{nameof(Operation.Direction)},
                                                        @{nameof(Operation.Datetime)},
                                                        @{nameof(Operation.Status)},
                                                        @{nameof(Operation.Type)},
                                                        @{nameof(Operation.AmountCurrency)},
                                                        @{nameof(Operation.IsSbpOperation)},
                                                        @{nameof(Operation.CategoryName)})
                                                        RETURNING operationId";

        private const string RatingQuery = @"SELECT o.title, CAST(SUM(o.amount) AS REAL) as amount FROM operations o WHERE direction = 'in' GROUP BY o.title ORDER BY amount DESC";

        private const string SelectQuery = @"SELECT id, package, org_id as OrganizationId, well_id as WellId, is_send as IsSend, date
                                                FROM wits_messages where is_send = false";

        private const string UpdateQuery = @"update wits_messages set is_send = true where id = :mId";

        private const string SetNotSentSql = @"UPDATE wits_messages SET is_send = FALSE WHERE date >= :timeFrom AND date <= :timeTo AND well_id = :wellId";

        private const string DeleteSql = @"DELETE FROM wits_messages WHERE date < @timeTo AND well_id = @wellId AND is_send = true";

        private readonly string _dbName;

        /// <summary>
        /// Конструктор с параметрами
        /// </summary>
        /// <param name="dbName">Название файла БД</param>
        public SqliteYooDbProvider(string dbName)
        {
            _dbName = dbName ?? throw new ArgumentNullException(nameof(dbName));
        }

        /// <summary>
        /// Добавить новую запись в бд
        /// </summary>
        /// <param name="msg">Сообщение из wits0</param>
        public async Task<List<Operation>> AddOperationsAsync(IList<Operation> operations)
        {
            var sentOps = new List<Operation>();
            using (var connection = new SqliteConnection(_dbName))
            {
                foreach (var operation in operations)
                {
                    var opId = await connection.QueryFirstOrDefaultAsync<string>(InsertQuery, operation);
                    if (!string.IsNullOrEmpty(opId))
                    {
                        sentOps.Add(operation);
                    }
                }
            }

            return sentOps;
        }

        /// <summary>
        /// Получить все не отправленные сообщения
        /// </summary>
        /// <returns>Список сообщений</returns>
        public Task<IList<Operation>> Get()
        {
            using (var connection = new SqliteConnection(_dbName))
            {
                 connection.Query<Operation>(SelectQuery).AsList();
                 return (Task<IList<Operation>>)Task.CompletedTask;
            }
        }
    
        public async Task<string> GetRatingAsync()
        {            
            var ratingStr = new StringBuilder();
            using (var connection = new SqliteConnection(_dbName))
            {
                var rating = await connection.QueryAsync<(string title, double amount)>(RatingQuery);
                foreach (var val in rating)
                {
                    ratingStr.Append(val.title);
                    ratingStr.Append(": ");
                    ratingStr.AppendLine(val.amount.ToString());
                }
            }

            return ratingStr.ToString();
        }
    }
}