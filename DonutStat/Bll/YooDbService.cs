using System.Text;
using DonutStat.Dal;
using DonutStat.Model;
using Microsoft.Extensions.Options;
using Telegram.Bots;
using Telegram.Bots.Requests;
using YooMoney;
using YooMoney.Model;

namespace DonutStat.Bll;

public class YooDbService : IYooDbService
{
    private readonly IYooDbProvider _provider;
    private readonly IBankClient _client;

    public YooDbService(IYooDbProvider provider, IBankClient client)
    {
        _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        _client = client ?? throw new ArgumentNullException(nameof(client));
    }

    public async Task<List<string>> RefreshOperationsAsync()
    {
        var donateStr = new StringBuilder();
        var paymentStr = new StringBuilder();
        var balanceStr = new StringBuilder();
        var hist = await _client.GetOperationHistAsync();
        if(hist is not null)
        {
            var insertedOps = await _provider.AddOperationsAsync(hist.Operations);
            var donates = insertedOps.Where(iO => iO.In);
            var payments = insertedOps.Where(iO => iO.Out);

            var hasDonate = donates.Any();
            if (hasDonate)
            {
                donateStr.AppendLine("💸 Список пополнений:");
                foreach(var operation in donates)
                {
                    donateStr.Append(operation.Title);
                    donateStr.Append(": ");
                    donateStr.AppendLine(operation.Amount.ToString());
                }
            }

            var hasPayment = payments.Any();
            if (hasPayment)
            {
                paymentStr.AppendLine("🧾 Чек по покупкам:");
                foreach(var operation in payments)
                {
                    paymentStr.Append(operation.Title);
                    paymentStr.Append(": ");
                    paymentStr.AppendLine(operation.Amount.ToString());
                }
            }

            if (hasPayment || hasDonate)
            {
                var balance = await _client.GetAccountInfoAsync();
                if (balance is not null)
                {
                    balanceStr.Append("💰 Доступно: ");
                    balanceStr.Append(balance.Balance);
                    balanceStr.Append("руб.");
                }
            }
        }

        return new List<string> { donateStr.ToString(), paymentStr.ToString(), balanceStr.ToString() };
    }

    public async Task<List<string>> GetRatingAsync()
    {
        var messages = await RefreshOperationsAsync();
        messages.Add(await _provider.GetRatingAsync());
        return messages;
    }
    public Task<IList<Operation>> Get()
    {
        throw new NotImplementedException();
    }
}
