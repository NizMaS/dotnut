// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IYooDbService.cs" company="TETA">
// Copyright (c) TETA. Ufa, 2020.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
// <summary>
//  Интерфейс провайдера доступа к локальной бд
// </summary>

using System.Collections.Generic;
using YooMoney.Model;

namespace DonutStat.Bll
{
    /// <summary>
    /// Интерфейс провайдера доступа к локальной бд
    /// </summary>
    public interface IYooDbService
    {
        /// <summary>
        /// Добавить новую запись в бд
        /// </summary>
        /// <param name="msg">Сообщение из wits0</param>
        Task<List<string>> RefreshOperationsAsync();

        /// <summary>
        /// Получить все не отправленные сообщения
        /// </summary>
        /// <returns>Список сообщений</returns>
        Task<IList<Operation>> Get();

        Task<List<string>> GetRatingAsync();
    }
}