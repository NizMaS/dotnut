using System.Text;
using DonutStat.Bll;
using DonutStat.Dal;
using Telegram.Bots;
using Telegram.Bots.Extensions.Polling;
using Telegram.Bots.Requests;
using Telegram.Bots.Types;
using YooMoney;

namespace DonutStat;

public sealed class TelegramHandler : IUpdateHandler
{
    private readonly IBankClient _client;
    private readonly IYooDbService _service;

    public TelegramHandler(IBankClient client, IYooDbService service)
    {
        _client = client ?? throw new ArgumentNullException(nameof(client));
        _service = service ?? throw new ArgumentNullException(nameof(service));
    }
    
    public async Task HandleAsync(IBotClient bot, Update update, CancellationToken token)
    {
        // var accInfo = _client.GetAccountInfo();
        switch (update)
        {
            case MessageUpdate u when u.Data is TextMessage msg:
                await ResponseToMsg(msg);
                break;

            // EditedMessageUpdate u when u.Data is TextMessage message =>
            //     bot.HandleAsync(new SendText(message.Chat.Id, message.Text)
            //     {
            //     ReplyToMessageId = message.Id
            //     }, token),

            default:
                await Task.CompletedTask;
                break;
        };

        async Task ResponseToMsg(TextMessage msg)
        {
            if (msg.Text.StartsWith("/"))
            {
                List<string> responses = new List<string>();
                switch(msg.Text)
                {
                    case "/balance": 
                        var accInfo = await _client.GetAccountInfoAsync();
                        responses.Add(accInfo is not null ? "Баланс: " + accInfo.Balance.ToString() + " рублей" : "Не удалось узнать баланс");
                        break;

                    case "/rating":
                        responses = await _service.GetRatingAsync();
                        break;

                    // case "/details": 
                    //     var details = await _client.GetOperationHistAsync();
                    //     var opsStr = new StringBuilder();
                    //     if(details is not null && details.Operations.Any())
                    //     {
                    //         await _provider.AddOperationsAsync(details.Operations);
                    //         resp = opsStr.ToString();
                    //     }
                    //     else
                    //     {
                    //         resp = "Не удалось запросить операции";
                    //     }
                    //     break;
                    default:
                        responses.Add("Сам ты" + msg.Text.Replace("/", " "));
                        break;
                }

                foreach (var resp in responses)
                {
                    await bot.HandleAsync(new SendText(msg.Chat.Id, resp){ReplyToMessageId = msg.Id}, token);
                }
            }
        }
    }
}
