using System.ComponentModel.DataAnnotations;

namespace DonutStat.Model;

public class YooOptions
{
    [Required(ErrorMessage = "Требуется указать ключ доступа")]
    public string Key { get; set; }
}
