using System.ComponentModel.DataAnnotations;

namespace DonutStat.Model
{
    public class  QuartzOptions
    {
        public string SchedulerId { get; set; }

        [Required(ErrorMessage = "Требуется указать периодчность запросов")]
        public int IntervalInSeconds { get; set; }
    }
}