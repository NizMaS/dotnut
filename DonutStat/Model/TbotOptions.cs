using System.ComponentModel.DataAnnotations;

namespace DonutStat.Model;

public class TbotOptions
{
    [Required(ErrorMessage = "Работа приложения без ключа телеграм-бота невозможна")]
    public string Key { get; set; }

    [Required(ErrorMessage = "Работа приложения без идентификатора чата невозможна")]
    public Int64 ChatId { get; set; }

    [Required(ErrorMessage = "Работа приложения без админа невозможна")]
    public Int64[] Admins { get; set; }
}
