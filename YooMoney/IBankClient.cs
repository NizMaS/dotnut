using YooMoney.Model;

namespace YooMoney;

public interface IBankClient
{

    /// <summary>
    /// Получение информации аккаунта (баланс)
    /// </summary>
    Task<AccountInfo?> GetAccountInfoAsync();

    /// <summary>
    /// Получить историю операций
    /// </summary>
    /// <param name="type">Тип(входящий/исходящий)</param>
    /// <param name="label">Лейбл</param>
    /// <param name="from">Начальное время запрашиваемого диапазона</param>
    /// <param name="till">Конечное время запрашиваемого диапазона</param>
    /// <param name="start_record">Начальная запись</param>
    /// <param name="records">Количество запрашиваемых записей</param>
    /// <param name="details">Подробная детализация</param>
    Task<OperationHistory?> GetOperationHistAsync(
        OpType type = OpType.None,
        string label = "",
        DateTime? from = null,
        DateTime? till = null,
        uint? start_record = null,
        uint? records = null,
        bool details = false
    );
}
