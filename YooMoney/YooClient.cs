using System;
using System.Net;
using System.Text.Json;
using YooMoney.Model;

namespace YooMoney;

/// <summary>
/// Клиент для вызовы методов Api yoomoney
/// </summary>
public class YooClient : IBankClient
{
    private const string _timeFormat = "yyyy-MM-ddTHH:mm:ssZ";
    private readonly HttpClient _client;
    public YooClient(HttpClient client)
    {
        _client = client ?? throw new ArgumentNullException(nameof(client));
    }

    /// <inheritdoc/>
    public async Task<AccountInfo?> GetAccountInfoAsync()
    {
        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "account-info");        
        HttpResponseMessage response = await _client.SendAsync(request);
        if(response.StatusCode == HttpStatusCode.OK)
        {
            HttpContent responseContent = response.Content;
            var json = await responseContent.ReadAsStringAsync();
            Console.WriteLine(json);
            return JsonSerializer.Deserialize<AccountInfo>(json);
        }
        return null;
    }

    /// <inheritdoc/>
    public async Task<OperationHistory?> GetOperationHistAsync
    (
        OpType type = OpType.None,
        string label = "",
        DateTime? from = null,
        DateTime? till = null,
        uint? start_record = null,
        uint? records = null,
        bool details = false
    )
    {
        HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "operation-history");
        
        var content = new Dictionary<string,string>();
        if(type == OpType.Payment)
        {
            content["type"] = "payment";
        }
        else if (type == OpType.Deposition)
        {
            content["type"] = "deposition";
        }

        if(!string.IsNullOrEmpty(label))
        {
            content["label"] = label;
        }

        if(from.HasValue)
        {
            content["from"] = from.Value.ToString(_timeFormat);
        }

        if(till.HasValue)
        {
            content["till"] = till.Value.ToString(_timeFormat);
        }

        if(start_record.HasValue && start_record != 0)
        {
            content["start_record"] = start_record.Value.ToString();
        }

        if(records.HasValue)
        {
            content["records"] = records.Value.ToString();
        }

        if(details)
        {
            content["details"] = "true";
        }

        if(content.Any())
        {
            request.Content = new FormUrlEncodedContent(content);
        }

        HttpResponseMessage response = await _client.SendAsync(request);
        if(response.StatusCode == HttpStatusCode.OK)
        {
            HttpContent responseContent = response.Content;
            var json = await responseContent.ReadAsStringAsync();
            Console.WriteLine(json);
            return JsonSerializer.Deserialize<OperationHistory>(json);
        }
        return null;
    }
}
