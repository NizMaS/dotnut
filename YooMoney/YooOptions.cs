using System.ComponentModel.DataAnnotations;

namespace YooMoney;

public class YooOptions
{
    [Required(ErrorMessage = "Требуется указать ключ доступа")]
    public string Key { get; set; }
}
