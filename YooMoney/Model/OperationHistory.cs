using System.Text.Json.Serialization;

namespace YooMoney.Model;

public class OperationHistory
{
    [JsonPropertyName("next_record")]
    public string NextRecord {get; set;}

    [JsonPropertyName("operations")]
    public List<Operation> Operations {get; set;}
}
