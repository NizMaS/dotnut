namespace YooMoney.Model;

/// <summary>
/// Тип запрашиваемых операций
/// </summary>
public enum OpType
{
    /// <summary>
    /// Не определено
    /// </summary>
    None = 0,

    /// <summary>
    /// пополнение счета (приход)
    /// </summary>
    Deposition = 1,

    /// <summary>
    /// платежи со счета (расход)
    /// </summary>
    Payment = 2
}
