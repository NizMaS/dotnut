using System.Text.Json.Serialization;

namespace YooMoney.Model;

public class BalanceDetails
{
    [JsonPropertyName("total")]
    public double Total {get; set;}

    [JsonPropertyName("available")]
    public double Available {get; set;}
}
