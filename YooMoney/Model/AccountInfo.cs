using System.Text.Json.Serialization;

namespace YooMoney.Model;

public class AccountInfo
{
    [JsonPropertyName("account")]
    public string Account {get; set;}

    [JsonPropertyName("balance")]
    public double Balance {get; set;}

    [JsonPropertyName("currency")]
    public string Currency {get; set;}

    [JsonPropertyName("account_type")]
    public string AccountType {get; set;}

    [JsonPropertyName("identified")]
    public bool Identified {get; set;}

    [JsonPropertyName("account_status")]
    public string AccountStatus {get; set;}

    [JsonPropertyName("balance_details")]
    public BalanceDetails BalanceDetails {get; set;}
}
