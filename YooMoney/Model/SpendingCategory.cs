using System.Text.Json.Serialization;

namespace YooMoney.Model;

public class SpendingCategory
{
    [JsonPropertyName("name")]
    public string Name {get; set;}

    [JsonPropertyName("sum")]
    public double Sum {get; set;}
}
