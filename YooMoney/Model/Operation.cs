using System.Text.Json.Serialization;

namespace YooMoney.Model;

public class Operation
{
    /// <summary>
    /// Информация о способе отправки денег
    /// </summary>
    [JsonPropertyName("details")]
    public string Details {get; set;}

    /// <summary>
    /// Название шаблона или номер счёта
    /// </summary>
    [JsonPropertyName("group_id")]
    public string GroupId {get; set;}

    /// <summary>
    /// Идентификатор операции в системе банка
    /// </summary>
    [JsonPropertyName("operation_id")]
    public string OperationId {get; set;}

    /// <summary>
    /// Отправитель или получатель средств
    /// </summary>
    [JsonPropertyName("title")]
    public string Title {get; set;}

    /// <summary>
    /// Сумма на счёте
    /// </summary>
    [JsonPropertyName("amount")]
    public double Amount {get; set;}

    /// <summary>
    /// Направление движения средств(in/out)
    /// </summary>
    [JsonPropertyName("direction")]
    public string Direction {get; set;}

    /// <summary>
    /// Является ли перевод средств входящим
    /// </summary>
    [JsonIgnore]
    public bool In 
    {
        get
        {
            return string.Equals(Direction, "in");
        }
    }



    /// <summary>
    /// Является ли перевод средств исходящим
    /// </summary>
    [JsonIgnore]
    public bool Out 
    {
        get
        {
            return string.Equals(Direction, "out");
        }
    }

    /// <summary>
    /// Время исполнения операции
    /// </summary>
    [JsonPropertyName("datetime")]
    public DateTime Datetime {get; set;}

    /// <summary>
    /// Статус выполнения операции (success)
    /// </summary>
    [JsonPropertyName("status")]
    public string Status {get; set;}

    /// <summary>
    /// Тип транзакции (оплата/пополнение)
    /// </summary>
    [JsonPropertyName("type")]
    public string Type {get; set;}

    /// <summary>
    /// Категория
    /// </summary>
    [JsonPropertyName("spendingCategories")]
    public List<SpendingCategory> SpendingCategories {get; set;}

    /// <summary>
    /// Имя категории (для работы с БД)
    /// </summary>
    [JsonIgnore]
    public string CategoryName {
        get
        {
            if (SpendingCategories.Any())
            {
                return SpendingCategories[0].Name;
            }
            return "None";
        }
        set
        {
            if (SpendingCategories.Any())
            {
                SpendingCategories[0].Name = value;
            }
            else
            {
                SpendingCategories = new List<SpendingCategory>() {new SpendingCategory() {Name = value}};
            }
        }
    }

    /// <summary>
    /// Валюта
    /// </summary>
    [JsonPropertyName("amount_currency")]
    public string AmountCurrency {get; set;}

    /// <summary>
    /// Через систему быстрых платежей
    /// </summary>
    [JsonPropertyName("is_sbp_operation")]
    public bool IsSbpOperation {get; set;}

    /// <summary>
    /// Доступные операции (техническая информация)
    /// </summary>
    [JsonPropertyName("available_operations")]
    public List<string> AvailableOperations {get; set;}
}
